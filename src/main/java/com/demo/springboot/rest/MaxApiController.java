package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/api")
public class MaxApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MaxApiController.class);

    @RequestMapping(value = "/max/{s}", method = RequestMethod.GET)
    public ResponseEntity<MaxDataDto> test(@PathVariable("s") String s) {
        LOGGER.info("--- run server status");
        MaxDataDto maxDataDto = new MaxDataDto(s);
        LOGGER.info("Max z podanego ciagu wynosi: {}" , maxDataDto.getMax());
        LOGGER.info("Min z podanego ciagu wynosi: {}" , maxDataDto.getMin());
        return new ResponseEntity<>(maxDataDto, HttpStatus.OK);
    }
    //wersja z wstrzykiwaniem
    @Autowired
    MaxDataDto maxDataDto;
    @RequestMapping(value = "/maxwired/{s}", method = RequestMethod.GET)
    public ResponseEntity<MaxDataDto> test2(@PathVariable("s") String s) {
        LOGGER.info("--- run server status");
        maxDataDto.setListOfDigits(s);
        LOGGER.info("Max z podanego ciagu wynosi: {}" , maxDataDto.getMax());
        LOGGER.info("Min z podanego ciagu wynosi: {}" , maxDataDto.getMin());
        return new ResponseEntity<>(maxDataDto, HttpStatus.OK);
    }
    //parametry
    @Autowired
    private MaxDataDto maxDataDto2;
    @RequestMapping(value = "/maxwired", method = RequestMethod.GET)
    public ResponseEntity<MaxDataDto> test3(@RequestParam(value = "s", required = true) String s) {
        LOGGER.info("--- run server status");
        maxDataDto2.setListOfDigits(s);
        LOGGER.info("Max z podanego ciagu wynosi: {}" , maxDataDto2.getMax());
        LOGGER.info("Min z podanego ciagu wynosi: {}" , maxDataDto2.getMin());
        return new ResponseEntity<>(maxDataDto2, HttpStatus.OK);
    }
}
