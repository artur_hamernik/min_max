package com.demo.springboot.domain.dto;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
@Service
public class MaxDataDto {



    private String listOfDigits;
    private Double min;
    private Double max;


    public MaxDataDto() { }

    public MaxDataDto(String listOfDigits) {
        this.listOfDigits = listOfDigits;
        calculateMax();
        calculateMin();
    }

    private void calculateMax() {
        Double max;
        List<Double> l = new ArrayList<Double>();
        char [] temp = listOfDigits.toCharArray();
        for(int i = 0; i < temp.length; i++){
            if(temp[i] == '-' && Character.isDigit(temp[i+1])){
                String po = "-";
                i++;
                while(i < temp.length && (Character.isDigit(temp[i]) || temp[i] == '.' && Character.isDigit(temp[i+1]))){
                    po += temp[i++];
                }
                l.add(Double.parseDouble(po));
            }
            else if(Character.isDigit(temp[i]) ){
                String po = "";
                while(i < temp.length && (Character.isDigit(temp[i]) || temp[i] == '.' && Character.isDigit(temp[i+1]))){
                    po += temp[i++];
                }
                l.add(Double.parseDouble(po));
            }
        }
        Collections.sort(l);
        this.max = l.get(l.size()-1);
    }
    private void calculateMin() {
        List<Double> l = new ArrayList<Double>();
        char [] temp = listOfDigits.toCharArray();
        for(int i = 0; i < temp.length; i++){
            if(temp[i] == '-' && Character.isDigit(temp[i+1])){
                String po = "-";
                i++;
                while(i < temp.length && (Character.isDigit(temp[i]) || temp[i] == '.' && Character.isDigit(temp[i+1]))){
                    po += temp[i++];
                }
                l.add(Double.parseDouble(po));
            }
            else if(Character.isDigit(temp[i]) ){
                String po = "";
                while(i < temp.length && (Character.isDigit(temp[i]) || temp[i] == '.' && Character.isDigit(temp[i+1]))){
                    po += temp[i++];
                }
                l.add(Double.parseDouble(po));
            }
        }
        Collections.sort(l);
        this.min = l.get(0);
    }
    public void setListOfDigits(String listOfDigits) {
        this.listOfDigits = listOfDigits;
        calculateMin();
        calculateMax();
    }
    public Double getMax() {
        return max;
    }
    public Double getMin() {
        return min;
    }

}